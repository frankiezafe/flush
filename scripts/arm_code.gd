extends Node

export(float, 0, 100) var norm_min = 0
export(float, 0, 100) var norm_max = 0

var skin = null

func _ready():
	skin = get_node( "../arm" )
	pass # Replace with function body.

func _process(delta):
	
	if not skin.is_class("Skin") :
		skin = get_node( "../arm" )
	
	skin.normal_length = rand_range( norm_min, norm_max )
	