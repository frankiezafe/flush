extends Node

export(String) var root_path = ""
export(float) var delay_pressed = 0.15

var UID_PREFIX = "device."
var UID = 0

var menu_tmpl = null
var menus = []
var menu_selected = []
var all_elements = []
var dict_elements = {}

var keys = [KEY_UP,KEY_RIGHT,KEY_DOWN,KEY_LEFT]
var gamepad_actions = ["gp_cross_up","gp_cross_right","gp_cross_down","gp_cross_left"]
var pressed = [false, false, false, false]
var delays = [0,0,0,0]

func scan_skin( info ):
	
	var skin = info['obj']
	
	for g in skin.get_dot_groups():
		var group = skin.get_dots( g )
		var ginfo = { 
			'name':g,
			'type':'dots',
			'parent': skin,
			'UID': 'device.' + str(UID),
			'params' : [
				{'name':'damping', 'type':'param', 'init':group.get_damping(), 'obj':group }, 
				{'name':'pressure', 'type':'param', 'init':group.get_pressure(), 'obj':group}],
			'last_selected': 0
		}
		info['children'].append( ginfo )
		UID += 1
	
	for g in skin.get_fiber_groups():
		var group = skin.get_fibers( g )
		var ginfo = { 
			'name':g,
			'type':'fibers',
			'parent': skin,
			'UID': 'device.' + str(UID),
			'params' : [
				{'name':'stiffness', 'type':'param', 'init':group.get_stiffness(), 'obj':group }, 
				{'name':'shrink', 'type':'param', 'init':group.get_shrink(), 'obj':group}],
			'last_selected': 0
		}
		info['children'].append( ginfo )
		UID += 1
	
	for g in skin.get_ligament_groups():
		var group = skin.get_ligaments( g )
		var ginfo = { 
			'name':g,
			'type':'ligaments',
			'parent': skin,
			'UID': 'device.' + str(UID),
			'params' : [
				{'name':'stiffness', 'type':'param', 'init':group.get_stiffness(), 'obj':group }, 
				{'name':'shrink', 'type':'param', 'init':group.get_shrink(), 'obj':group}],
			'last_selected': 0
		}
		info['children'].append( ginfo )
		UID += 1

func scan_tree( element, parent=null, skip=false ):
	
	if element == null:
		print( "failed to scan_tree!" )
		return
	
	var ename = element.name
	ename = ename.replace( '@', 'O' )
	ename = ename.replace( '_', '.' )
	for i in range(0,9):
		ename = ename.replace( str(i), char(63+i) )
	
	var info = { 
		'name':ename,
		'type':'obj',
		'parent': parent, 
		'obj':element,
		'class':null, 
		'UID': 'device.' + str(UID),
		'children': [],
		'params' : [
			{'name':'vi', 'type':'param','init':true, 'obj':element }, 
			{'name':'scale', 'type':'param', 'init':null, 'obj':element}, 
			{'name':'position', 'type':'param', 'init':null, 'obj':element},
			{'name':'rotation', 'type':'param', 'init':null, 'obj':element} ],
		'last_selected': 0
	}
	
	if element == self:
		return
	
	if element.is_class( "Skin" ):
		info['class'] = "Skin"
		var p = info['params'].duplicate()
		info['params'] = []
		info['params'].append( {'name':'normal_length','type':'param','init':element.normal_length,'obj':element} )
		info['params'].append( {'name':'display_skin','type':'param','init':element.display_skin,'obj':element} )
		info['params'] += p
		info['params'][3]['init'] = element.global_transform.basis.get_scale()
		info['params'][4]['init'] = element.global_transform.origin
		info['params'][5]['init'] = element.global_transform.basis.get_euler()
		scan_skin( info )
		
	elif element.is_class( "Camera" ):
		info['class'] = "Camera"
		var p = info['params'].duplicate()
		info['params'] = [
			{'name':'fov','type':'param','init':element.fov,'obj':element},
			{'name':'locked','type':'param','init':true,'obj':element},
			p[2],p[3]]
		info['params'][2]['init'] = element.global_transform.origin
		info['params'][3]['init'] = element.global_transform.basis.get_euler()
		
	elif element.is_class( "Spatial" ):
		info['class'] = "Spatial"
		info['params'][1]['init'] = element.global_transform.basis.get_scale()
		info['params'][2]['init'] = element.global_transform.origin
		info['params'][3]['init'] = element.global_transform.basis.get_euler()
		
	elif element.is_class( "Light" ):
		info['class'] = "Light"
		info['params'][1]['init'] = element.global_transform.basis.get_scale()
		info['params'][2]['init'] = element.global_transform.origin
		info['params'][3]['init'] = element.global_transform.basis.get_euler()
	
	if not skip and info['class'] != null:
		if parent == null:
			all_elements.append( info )
		else:
			parent['children'].append( info )
		dict_elements[info['UID']] = info
		UID += 1
		
	elif not skip and info['class'] != null:
		return
	
	if skip:
		info = null
	
	var childs = element.get_children()
	for c in childs:
		scan_tree( c, info )

func menu_positions():
	var offsetx = 0
	var offsety = 0
	for mi in range(len(menus)):
		var m = menus[mi]
		if mi == 0:
			m.position = Vector2( 0, get_viewport().size.y * 0.5 - m.get_size().y * 0.5 )
			offsety = m.position.y + m.selected * m.label_height
		else:
			m.position = Vector2( offsetx, offsety - m.selected * m.label_height )
		m.marker.position.y = m.selected * m.label_height + m.marker_offset_y
		offsetx += m.get_size().x

func add_menu( element=null ):
	
	var elements = []
	var jump_to = 0
	if element == null:
		elements = all_elements
	else:
		if 'last_selected' in element:
			jump_to = element['last_selected']
			print( 'jump_to ', jump_to )
		if 'children' in element:
			elements += element['children']
		if 'params' in element:
			elements += element['params']
	
	if len( elements ) == 0:
		return
	
	var m = menu_tmpl.duplicate()
	m.visible = false
	add_child( m )
	menus.append( m )
	m.init( elements, jump_to )
	menu_positions()
	retrieve_selected()

func remove_submenu():
	if len(menus) == 0:
		return
	var lastm = menus[len(menus)-1]
	menus.erase( lastm )
	remove_child( lastm )
	retrieve_selected()

func retrieve_selected():
	
	# recreating the list of selected items
	menu_selected = []
	for m in menus:
		menu_selected.append( m.selected )

func get_current_element():
	if len( menu_selected ) == 0:
		return null
	var e = null
	for mid in menu_selected:
		if e == null:
			e = all_elements[mid]
		else:
			if 'children' in e and mid < len(e['children']):
				e = e['children'][mid]
			elif 'params' in e:
				var pid = mid
				if 'children' in e:
					pid -= len(e['children'])
				if pid < len(e['params']):
					e = e['params'][pid]
				else:
					e = null
					break
	return e

func _ready():
	menu_tmpl = get_node( "menu_tmpl" )
	UID = 0
	scan_tree( get_node(root_path), null, true )
	# first lvl menu
	add_menu()
	self.visible = false

func _input(event):
	pass

func _process(delta):
	
	if len(menus) == 0:
		return
	
	for i in range(4):
		pressed[i] = false
		if ( Input.is_key_pressed( keys[i] ) or Input.is_action_pressed(gamepad_actions[i]) ) and delays[i] <= 0 and not pressed[i]:
			pressed[i] = true
			delays[i] = delay_pressed
		elif delays[i] > 0:
			delays[i] -= delta
	
	if Input.is_action_just_pressed( "select_button" ):
		self.visible = !self.visible
	
	if not self.visible:
		for i in range(4):
			if pressed[i]:
				self.visible = true
				break
		if not self.visible:
			return
	
	menu_positions()
	
	var mlen = len(menus)
	var leaf = menus[mlen-1]
	var branch = null
	if len(menus) > 1:
		branch = menus[mlen-2].get_element()
	
	if pressed[3]:
		if len(menus) > 1:
			remove_submenu()
		else:
			self.visible = false
			return
	elif pressed[1]:
		var sel = leaf.get_element()
		add_menu( sel )
	
	if pressed[0] or pressed[2]:
		if pressed[0]:
			leaf.update_menu(delta, KEY_UP)
		else:
			leaf.update_menu(delta, KEY_DOWN)
		if branch != null and 'last_selected' in branch:
			branch['last_selected'] = leaf.selected
		retrieve_selected()
