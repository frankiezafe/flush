extends Node

var deadZone = 0.2
export(float, 0, 100) var camspeed = 3
var fullscreen = false

func joy_con_changed(deviceid,isConnected):
	if isConnected:
		print("Joystick " + str(deviceid) + " connected")
		if Input.is_joy_known(0):
			print("Recognized and compatible joystick")
			print(Input.get_joy_name(0) + " device connected")
		else:
			print("Joystick " + str(deviceid) + " disconnected")

func analog_stabilisator( joyid, xaxis, yaxis ):
	var axis = Vector2(Input.get_joy_axis(0,xaxis),Input.get_joy_axis(0,yaxis))
	if abs( axis.x ) < deadZone:
		axis.x = 0
	elif axis.x < 0:
		axis.x = ( axis.x + deadZone ) / ( 1 - deadZone )
	elif axis.x > 0:
		axis.x = ( axis.x - deadZone ) / ( 1 - deadZone )
	if abs( axis.y ) < deadZone:
		axis.y = 0
	elif axis.y < 0:
		axis.y = ( axis.y + deadZone ) / ( 1 - deadZone )
	elif axis.y > 0:
		axis.y = ( axis.y - deadZone ) / ( 1 - deadZone )
	return axis

func _ready():
	pass

func _process(delta):

	if Input.get_connected_joypads().size() > 0:
		
		var push
		
		push = analog_stabilisator( 0, JOY_ANALOG_LX, JOY_ANALOG_LY )
		
#		var ac = get_node("../arm_code")
#		ac.norm_min += push.x * delta
#		ac.norm_max += push.y * delta
		
		var cam = get_node("../cam" )
		if cam != null:
			push = analog_stabilisator( 0, JOY_ANALOG_RX, JOY_ANALOG_RY )
			cam.translation += Vector3( push.y, 0, push.x ) * delta * camspeed
			cam.look_at( Vector3(0,0,0), Vector3(0,1,0) )
		
#		if Input.is_action_just_pressed("gp_X_button"):
#		if Input.is_joy_button_pressed(0,JOY_BUTTON_0): # Same as JOY_XBOX_A
#			var ac = get_node("../arm_code")
#			ac.norm_min += .1 * delta
#			ac.norm_max += .2 * delta
#
#		if Input.is_joy_button_pressed(0,JOY_BUTTON_1): # Same as JOY_XBOX_A
#			var ac = get_node("../arm_code")
#			ac.norm_min -= .1 * delta
#			ac.norm_max -= .2 * delta
#			if ac.norm_max < 0:
#				ac.norm_min = 0
#				ac.norm_max = 0
#		if Input.is_joy_button_pressed(0,JOY_BUTTON_2):
#			get_node("../ctlrs/base/mid").rotation_degrees.z += 4 * delta
#			get_node("../ctlrs/base/mid/tip").rotation_degrees.z += 4 * delta
#		if Input.is_joy_button_pressed(0,JOY_BUTTON_3):
#			get_node("../ctlrs/base/mid").rotation_degrees.z -= 4 * delta
#			get_node("../ctlrs/base/mid/tip").rotation_degrees.z -= 4 * delta
			
		if Input.is_action_just_pressed("start_button"):
			fullscreen = not fullscreen
			OS.window_fullscreen = fullscreen
