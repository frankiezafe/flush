# require ui object + ui_overlay.gd

extends Node

var deadZone = 0.2
export(float, 0, 100) var camspeed = 3
export(float, 0, 1) var damp = 0.1
var fullscreen = false
var push = Vector2(0,0)

var ui = null
var cam = null
var cam_lookat = null
var cam_uid = null

func joy_con_changed(deviceid,isConnected):
	if isConnected:
		print("Joystick " + str(deviceid) + " connected")
		if Input.is_joy_known(0):
			print("Recognized and compatible joystick")
			print(Input.get_joy_name(0) + " device connected")
		else:
			print("Joystick " + str(deviceid) + " disconnected")

func analog_stabilisator( joyid, xaxis, yaxis ):
	var axis = Vector2(Input.get_joy_axis(0,xaxis),Input.get_joy_axis(0,yaxis))
	if abs( axis.x ) < deadZone:
		axis.x = 0
	elif axis.x < 0:
		axis.x = ( axis.x + deadZone ) / ( 1 - deadZone )
	elif axis.x > 0:
		axis.x = ( axis.x - deadZone ) / ( 1 - deadZone )
	if abs( axis.y ) < deadZone:
		axis.y = 0
	elif axis.y < 0:
		axis.y = ( axis.y + deadZone ) / ( 1 - deadZone )
	elif axis.y > 0:
		axis.y = ( axis.y - deadZone ) / ( 1 - deadZone )
	return axis

func _ready():
	ui = get_node("../ui" )
	cam = get_node("../cam" )
	cam_lookat = get_node("../cam_lookat" )

func _process(delta):
	
	if len( ui.all_elements ) == 0:
		return
	
	if cam_uid == null:
		for e in ui.all_elements:
			if e['obj'] == self:
				cam_uid = e
	
	if Input.get_connected_joypads().size() > 0:
		
		if cam != null and cam_lookat != null:
			var cam_front = Vector3( 0,0,-1 )
			var cam_right = Vector3( 1,0,0 )
			var q = Quat()
			q.set_euler( cam.global_transform.basis.get_euler() )
			var t = Transform( q )
			cam_front = t.xform( cam_front )
			cam_right = t.xform( cam_right )
			push = push * damp + analog_stabilisator( 0, JOY_ANALOG_RX, JOY_ANALOG_RY ) * (1-damp)
			cam.translation += cam_front * -pow( push.y, 3 ) * delta * camspeed
			cam.translation += cam_right * pow( push.x, 3 ) * delta * camspeed
			cam.look_at( cam_lookat.global_transform.origin, Vector3(0,1,0) )

		if Input.is_action_just_pressed("start_button"):
			fullscreen = not fullscreen
			OS.window_fullscreen = fullscreen
