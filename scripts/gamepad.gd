# require ui with menu_tree_manager on it AND a cam!

extends Node

export(float, 0, 100) var trans_speed = 3
export(float, 0, 100) var rot_speed = 15
export(float, 0, 100) var scale_speed = 5
export(float, 0, 100) var normal_speed = 5
export(float, 0, 100) var fov_speed = 10
export(float, 0, 100) var damping_speed = 0.3
export(float, 0, 100) var pressure_speed = 0.3
export(float, 0, 100) var stiffness_speed = 0.3
export(float, 0, 100) var shrink_speed = 0.3
export(float, 0, 100) var cam_speed = 3
export(float, 0, 1) var cam_damp = 0.1

var ui = null
var cam = null
var cam_lookat = null
var cam_push = Vector2(0,0)
var cam_locked = true
var cam_active = true
var cam_free_speed = Vector3(0,0,0)
var cam_trust = 0

var deadZone = 0.2
var speedMultiplier = 3
var fullscreen = false

func _ready():
	ui = get_node( "../ui" )
	cam = get_node( "../cam" )
	cam_lookat = get_node( "../cam_lookat" )
  
func joy_con_changed(deviceid,isConnected):
	if isConnected:
		print("Joystick " + str(deviceid) + " connected")
		if Input.is_joy_known(0):
			print("Recognized and compatible joystick")
			print(Input.get_joy_name(0) + " device connected")
		else:
			print("Joystick " + str(deviceid) + " disconnected")

func analog_stabilisator( joyid, xaxis, yaxis ):
	var axis = Vector2(Input.get_joy_axis(0,xaxis),Input.get_joy_axis(0,yaxis))
	if abs( axis.x ) < deadZone:
		axis.x = 0
	elif axis.x < 0:
		axis.x = ( axis.x + deadZone ) / ( 1 - deadZone )
	elif axis.x > 0:
		axis.x = ( axis.x - deadZone ) / ( 1 - deadZone )
	if abs( axis.y ) < deadZone:
		axis.y = 0
	elif axis.y < 0:
		axis.y = ( axis.y + deadZone ) / ( 1 - deadZone )
	elif axis.y > 0:
		axis.y = ( axis.y - deadZone ) / ( 1 - deadZone )
	return axis

func _process(delta):

	if Input.get_connected_joypads().size() > 0:
		
		var v2
		
		var cam = get_node("../cam" )
		if cam != null:
			if cam_locked and cam_lookat != null:
				var cam_front = Vector3( 0,0,-1 )
				var cam_right = Vector3( 1,0,0 )
				var q = Quat()
				q.set_euler( cam.global_transform.basis.get_euler() )
				var t = Transform( q )
				cam_front = t.xform( cam_front )
				cam_right = t.xform( cam_right )
				cam_push = cam_push * cam_damp + analog_stabilisator( 0, JOY_ANALOG_RX, JOY_ANALOG_RY ) * (1-cam_damp)
				cam.translation += cam_front * -pow( cam_push.y, 3 ) * delta * cam_speed
				cam.translation += cam_right * pow( cam_push.x, 3 ) * delta * cam_speed
				cam.look_at( cam_lookat.global_transform.origin, Vector3(0,1,0) )
			else:
				v2 = analog_stabilisator( 0, JOY_ANALOG_RX, JOY_ANALOG_RY )
				
				var current_speed = Vector3( -v2.y, -v2.x, 0 ) * cam_speed * delta
				var current_trust = 0
				if cam_active:
					current_trust = ( pow( Input.get_joy_axis(0,JOY_L2), 2 ) - pow( Input.get_joy_axis(0,JOY_R2), 2 ) ) * cam_speed * delta
					
				cam_free_speed += ( current_speed - cam_free_speed ) * 0.4 * delta
				cam_trust += ( current_trust - cam_trust ) * 0.4 * delta
				
				cam.rotation += cam_free_speed
				var cam_front = Vector3(0,0,1)
				var q = Quat()
				q.set_euler( cam.global_transform.basis.get_euler() )
				var t = Transform( q )
				cam_front = t.xform( cam_front )
				cam.translation += cam_front * cam_trust
				
		
		if Input.is_action_just_pressed("start_button"):
			fullscreen = not fullscreen
			OS.window_fullscreen = fullscreen
			print( "pushed" )
		
		if ui != null:
			
			var sel = ui.get_current_element()
			if sel == null:
				cam_active = true
				return
			if sel['type'] != 'param':
				cam_active = true
				return
			
			v2 = analog_stabilisator( 0, JOY_ANALOG_LX, JOY_ANALOG_LY )
			
			if sel['obj'] == cam:
				cam_active = true
			else:
				cam_active = false
			
			if sel['name'] == 'vi':
				if Input.is_action_just_pressed("gp_A_button"):
					sel['obj'].visible = not sel['obj'].visible
			
			if sel['name'] == 'locked':
				if Input.is_action_just_pressed("gp_A_button"):
					cam_locked = not cam_locked
					if not cam_locked:
						cam_free_speed = Vector3()
						cam_trust = 0
					
			elif sel['name'] == 'fov' and v2.y != 0:
				sel['obj'].fov -= v2.y * fov_speed * delta
				if sel['obj'].fov <= 0.1:
					sel['obj'].fov = 0.1
			
			if sel['name'] == 'display_skin':
				if Input.is_action_just_pressed("gp_A_button"):
					sel['obj'].display_skin = not sel['obj'].display_skin
					
			elif sel['name'] == 'normal_length' and v2.y != 0:
				sel['obj'].normal_length -= v2.y * normal_speed * delta
				if sel['obj'].normal_length <= 0:
					sel['obj'].normal_length = 0
					
			elif sel['name'] == 'damping' and v2.y != 0:
				var d = sel['obj'].get_damping()
				d -= v2.y * damping_speed * delta
				if d < 0:
					d = 0
				sel['obj'].set_damping(d)
				
			elif sel['name'] == 'pressure' and v2.y != 0:
				var d = sel['obj'].get_pressure()
				d -= v2.y * pressure_speed * delta
				sel['obj'].set_pressure(d)
				
			elif sel['name'] == 'stiffness' and v2.y != 0:
				var d = sel['obj'].get_stiffness()
				d -= v2.y * stiffness_speed * delta
				if d < 0:
					d = 0
				sel['obj'].set_stiffness(d)
				
			elif sel['name'] == 'shrink' and v2.y != 0:
				var d = sel['obj'].get_shrink()
				d -= v2.y * shrink_speed * delta
				if d < 0:
					d = 0
				sel['obj'].set_shrink(d)
					
			elif sel['name'] == 'scale':
				var element_scale = sel['obj'].scale.x
				element_scale -= v2.y * scale_speed * delta
				if element_scale < 0.00001:
					element_scale = 0.00001
				sel['obj'].scale = Vector3( element_scale,element_scale,element_scale )
				
			elif sel['name'] == 'position':
				var v3 = Vector3( 
					v2.x, -v2.y,
					Input.get_joy_axis(0,JOY_L2) - Input.get_joy_axis(0,JOY_R2)
					) * trans_speed * delta
				v3 = cam.global_transform.xform( v3 )
				v3 -= cam.global_transform.origin
				sel['obj'].global_transform.origin += v3
				
			elif sel['name'] == 'rotation':
				var v3 = Vector3( 
					Input.get_joy_axis(0,JOY_L2) - Input.get_joy_axis(0,JOY_R2),
					v2.x, v2.y ) * trans_speed * delta
				sel['obj'].rotation += v3
				cam_active = false
			
			#print( sel['name'] )
		
#		var xAxis = Input.get_joy_axis(0,JOY_AXIS_7)
#		if abs(xAxis) > deadZone:
#			# xAxis is a value from +/0 0-1 depending on how hard the stick is being pressed
#			if xAxis < 0:
#				self.translation.x-= 1 * delta * ( speedMultiplier * abs(xAxis))
#			if xAxis > 0:
#				self.translation.x+= 1 * delta * ( speedMultiplier * abs(xAxis))
#		if Input.is_joy_button_pressed(0,JOY_BUTTON_0): # Same as JOY_XBOX_A
#			self.translation = startingPos
		# Buttons have different meanings on different devices
		# Let's loop through and see what they are defined as
#		for i in range(16):
#			if Input.is_joy_button_pressed(0,i):
#				print("Button at " + str(i) + " pressed, should be button: " + Input.get_joy_button_string(i))
#
#		if Input.is_joy_button_pressed(0,JOY_DPAD_UP):
#			self.translation.z -= 10
#		if Input.is_joy_button_pressed(0,JOY_DPAD_DOWN):
#			self.translation.z += 10
