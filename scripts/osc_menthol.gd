# require ui object + ui_overlay.gd

extends OSCreceiver

export(String) var group_conf_0 = "obj#type#name#value#address"
export(String) var group_conf_1 = "obj#type#name#value#address"
export(String) var group_conf_2 = "obj#type#name#value#address"
export(String) var group_conf_3 = "obj#type#name#value#address"
export(String) var group_conf_4 = "obj#type#name#value#address"
export(String) var group_conf_5 = "obj#type#name#value#address"
export(String) var group_conf_6 = "obj#type#name#value#address"
export(String) var group_conf_7 = "obj#type#name#value#address"

var ui = null
var q = Quat()
var b = Basis()
var group_generated = false
var groups = {}

func parse_group_config(g):
	if ui == null:
		print( "parse_group_config ui failed: ", g )
		return
	var gs = g.split("#")
	if len(gs) != 5:
		print( "parse_group_config gs failed: ", g )
		return
	var obj = null
	for e in ui.all_elements:
		if e['name'] == gs[0]:
			obj = e['obj']
			break
	if obj == null:
		print( "parse_group_config obj failed: ", g )
		return
	var ssg = null
	if gs[1] == "dot":
		ssg = obj.get_dots( gs[2] )
	elif gs[1] == "fiber":
		ssg = obj.get_fibers( gs[2] )
	elif gs[1] == "ligament":
		ssg = obj.get_ligaments( gs[2] )
	if ssg == null:
		print( "parse_group_config ssg failed: ", g )
		return
	if not gs[4] in groups:
		groups[gs[4]] = []
	groups[gs[4]].append( { 'value' : gs[3], 'group': ssg } )
	
func _ready():
	ui = get_node("../ui")

func parse_menthol( msg ):
	
	var addr = msg.address()
	
	var addr_keys = addr.split( "/" )
		
	if len(addr_keys) != 2:
		print( "???? ", addr )
		return
	#device.9/scale
	
	if not addr_keys[0] in ui.dict_elements:
		print( "unknow key: " + addr_keys[0] )
	
	var sel_e = ui.dict_elements[ addr_keys[0] ]
	
	# everything
	if addr_keys[1] == "translation" and msg.arg_num() == 3:
		sel_e['obj'].global_transform.origin = Vector3( msg.arg(0), msg.arg(1), msg.arg(2) )
	
	elif addr_keys[1] == "rotation" and msg.arg_num() == 3:
		q.set_euler( Vector3( msg.arg(0), msg.arg(1), msg.arg(2) ) )
		var sca = sel_e['obj'].global_transform.basis.get_scale()
		b = Basis(q)
		b = b.scaled( sca )
		sel_e['obj'].global_transform.basis = b
	
	elif addr_keys[1] == "scale" and msg.arg_num() == 3:
		q.set_euler( sel_e['obj'].global_transform.basis.get_euler() )
		b = Basis(q)
		b = b.scaled( Vector3( msg.arg(0), msg.arg(1), msg.arg(2) ) )
		sel_e['obj'].global_transform.basis = b
	
	# specialities
	elif sel_e['class'] == "Camera" and addr_keys[1] == "fov" and msg.arg_num() == 1:
		sel_e['obj'].fov = msg.arg(0)
	
	elif sel_e['class'] == "Skin":
		
		if addr_keys[1] == "display_skin" and msg.arg_num() == 1:
			if ( msg.arg(0) == 1 ):
				sel_e['obj'].display_skin = true
			else:
				sel_e['obj'].display_skin = false
		
		elif addr_keys[1] == "display_ligaments" and msg.arg_num() == 1:
			if ( msg.arg(0) == 1 ):
				sel_e['obj'].display_ligaments = true
			else:
				sel_e['obj'].display_ligaments = false
		
		elif addr_keys[1] == "display_normals" and msg.arg_num() == 1:
			if ( msg.arg(0) == 1 ):
				sel_e['obj'].display_normals = true
			else:
				sel_e['obj'].display_normals = false
		
		elif addr_keys[1] == "display_edges" and msg.arg_num() == 1:
			if ( msg.arg(0) == 1 ):
				sel_e['obj'].display_edges = true
			else:
				sel_e['obj'].display_edges = false
		
		elif addr_keys[1] == "normal_length" and msg.arg_num() == 1:
			sel_e['obj'].normal_length = msg.arg(0)
		
		elif addr_keys[1] == "gravity" and msg.arg_num() == 3:
			sel_e['obj'].gravity = Vector3( msg.arg(0), msg.arg(1), msg.arg(2) )
		
		else:
			var group_keys = addr_keys[1].split("#")
			if len( group_keys ) != 3:
				return
		
			# groups controls
			if group_keys[0] == "dots":
				var gname = group_keys[1]
				var gparam= group_keys[2]
				var dg = sel_e['obj'].get_dots( gname )
				if dg != null:
					if gparam == "damping":
						dg.set_damping( msg.arg(0) )
					elif gparam == "pressure":
						dg.set_pressure( msg.arg(0) )
			if group_keys[0] == "fibers":
				var gname = group_keys[1]
				var gparam= group_keys[2]
				var fg = sel_e['obj'].get_fibers( gname )
				if fg != null:
					if gparam == "shrink":
						fg.set_shrink( msg.arg(0) )
					elif gparam == "stiffness":
						fg.set_stiffness( msg.arg(0) )
			if group_keys[0] == "ligaments":
				var gname = group_keys[1]
				var gparam= group_keys[2]
				var lg = sel_e['obj'].get_ligaments( gname )
				if lg != null:
					if gparam == "shrink":
						lg.set_shrink( msg.arg(0) )
					elif gparam == "stiffness":
						lg.set_stiffness( msg.arg(0) )

#warning-ignore:unused_argument
func _process(delta):
	
	if len( ui.dict_elements ) == 0:
		return
	
	if not group_generated:
		parse_group_config( group_conf_0 )
		parse_group_config( group_conf_1 )
		parse_group_config( group_conf_2 )
		parse_group_config( group_conf_3 )
		parse_group_config( group_conf_4 )
		parse_group_config( group_conf_5 )
		parse_group_config( group_conf_6 )
		parse_group_config( group_conf_7 )
		group_generated = true
	
	while has_waiting_messages():
		
		var msg = get_next_message()
		
		var addr = msg.address()
		if addr.substr(0,len(ui.UID_PREFIX)) == ui.UID_PREFIX:
			parse_menthol( msg )
			continue
		
		if addr in groups:
			for g in groups[addr]:
				if g['value'] == "damping":
					g['group'].set_damping( msg.arg(0) )
				if g['value'] == "pressure":
					g['group'].set_pressure( msg.arg(0) )
				if g['value'] == "shrink":
					g['group'].set_shrink( msg.arg(0) )
				if g['value'] == "stiffness":
					g['group'].set_stiffness( msg.arg(0) )