# require ui object + ui_overlay.gd

extends OSCsender

export(bool) var send_methol = false
export(bool) var send_gamepad = false
export(bool) var send_distances = false

var deadZone = 0.2
var ui = null
var methol_sent = false

var cam = null
var cam_to_objects = []
var previous_distances = []

func analog_stabilisator( joyid, xaxis, yaxis ):
	var axis = Vector2(Input.get_joy_axis(joyid,xaxis),Input.get_joy_axis(joyid,yaxis))
	if abs( axis.x ) < deadZone:
		axis.x = 0
	elif axis.x < 0:
		axis.x = ( axis.x + deadZone ) / ( 1 - deadZone )
	elif axis.x > 0:
		axis.x = ( axis.x - deadZone ) / ( 1 - deadZone )
	if abs( axis.y ) < deadZone:
		axis.y = 0
	elif axis.y < 0:
		axis.y = ( axis.y + deadZone ) / ( 1 - deadZone )
	elif axis.y > 0:
		axis.y = ( axis.y - deadZone ) / ( 1 - deadZone )
	return axis

func osc_gamepad():
	var v2 = null
	v2 = analog_stabilisator( 0, JOY_ANALOG_LX, JOY_ANALOG_LY )
	msg_address( "/gamepad/analog_left" )
	msg_add_v2( v2 )
	msg_send()
	v2 = analog_stabilisator( 0, JOY_ANALOG_RX, JOY_ANALOG_RY )
	msg_address( "/gamepad/analog_right" )
	msg_add_v2( v2 )
	msg_send()
	msg_address( "/gamepad/buttons" )
	for i in range(JOY_BUTTON_MAX):
		if Input.is_joy_button_pressed(0,i):
			msg_add_int( 1 )
		else:
			msg_add_int( 0 )
	msg_send()
	msg_address( "/gamepad/cross" )
	if Input.is_action_pressed("gp_cross_up"):
		msg_add_int( 1 )
	else:
		msg_add_int( 0 )
	if Input.is_action_pressed("gp_cross_right"):
		msg_add_int( 1 )
	else:
		msg_add_int( 0 )
	if Input.is_action_pressed("gp_cross_down"):
		msg_add_int( 1 )
	else:
		msg_add_int( 0 )
	if Input.is_action_pressed("gp_cross_left"):
		msg_add_int( 1 )
	else:
		msg_add_int( 0 )
	msg_send()
	msg_address( "/gamepad/trigger" )
	msg_add_real( Input.get_joy_axis(0,JOY_L2) )
	msg_add_real( Input.get_joy_axis(0,JOY_R2) )
	msg_send()

func send_methol( s ):
	
	var ss = s.split( ' ' )
	for i in ss:
		i = i.replace('|',' ')
		msg_add_string( i )
	msg_address( "FLUSH" )
	msg_send()

func osc_menthol_element(base, e):
	
	var s
	var ss
	var v3
	
	var trans_range = "|@range|-100|100"
	var rot_range = "|@range|" + str( -PI ) + "|" + str( PI )
	var scale_range = "|@range|0|50"
	var fov_range = "|@range|0|180"
	var normal_range = "|@range|0|80"
	var damping_range = "|@range|0|1"
	var pressure_range = "|@range|-3|3"
	var shrink_range = "|@range|0|1"
	var stiffness_range = "|@range|0|1"
	
	s = base + e['name'] + " "
	ss =  s + "translation " + e['UID'] + " v3f" + trans_range + " "
	v3 = e['obj'].global_transform.origin
	ss += str( v3.x ) + "|"
	ss += str( v3.y ) + "|"
	ss += str( v3.z ) 
	send_methol( ss )
	
	ss =  s + "rotation " + e['UID'] + " v3f" + rot_range + " "
	v3 = e['obj'].global_transform.basis.get_euler()
	ss += str( v3.x ) + "|"
	ss += str( v3.y ) + "|"
	ss += str( v3.z ) 
	send_methol( ss )
	
	ss =  s + "scale " + e['UID'] + " v3f" + scale_range + " "
	v3 = e['obj'].global_transform.basis.get_scale()
	ss += str( v3.x ) + "|"
	ss += str( v3.y ) + "|"
	ss += str( v3.z ) 
	send_methol( ss )
	
	if e['class'] == "Camera":
		
		ss =  s + "fov " + e['UID'] + " f" + fov_range + " " + str(e['obj'].fov)
		send_methol( ss )
		
	elif e['class'] == "Skin":
		ss =  s + "display_skin " + e['UID'] + " bool "
		if  e['obj'].display_skin:
			ss += "1"
		else:
			ss += "0"
		send_methol( ss )
		
		ss =  s + "display_ligaments " + e['UID'] + " bool "
		if  e['obj'].display_ligaments:
			ss += "1"
		else:
			ss += "0"
		send_methol( ss )
		
		ss =  s + "display_normals " + e['UID'] + " bool "
		if  e['obj'].display_normals:
			ss += "1"
		else:
			ss += "0"
		send_methol( ss )
		
		ss =  s + "display_edges " + e['UID'] + " bool "
		if  e['obj'].display_edges:
			ss += "1"
		else:
			ss += "0"
		send_methol( ss )
		
		ss =  s + "normal_length " + e['UID'] + " f" + normal_range + " " + str(e['obj'].normal_length)
		send_methol( ss )
		
		ss =  s + "gravity " + e['UID'] + " v3f "
		v3 = e['obj'].global_transform.basis.get_scale()
		ss += str( v3.x ) + "|"
		ss += str( v3.y ) + "|"
		ss += str( v3.z )
		send_methol( ss )
		
		for dgn in e['obj'].get_dot_groups():
			var dg = e['obj'].get_dots( dgn )
			s = base + e['name'] + "/dots "
			ss =  s + "dots#" + dgn + "#damping " + e['UID'] + " f" + damping_range + " " + str(dg.get_damping())
			send_methol( ss )
			ss =  s + "dots#" + dgn + "#pressure " + e['UID'] + " f" + pressure_range + " " + str(dg.get_pressure())
			send_methol( ss )
		for fgn in e['obj'].get_fiber_groups():
			var fg = e['obj'].get_fibers( fgn )
			s = base + e['name'] + "/fibers "
			ss =  s + "fibers#" + fgn + "#shrink " + e['UID'] + " f" + shrink_range + " " + str(fg.get_shrink())
			send_methol( ss )
			ss =  s + "fibers#" + fgn + "#stiffness " + e['UID'] + " f" + stiffness_range + " " + str(fg.get_stiffness())
			send_methol( ss )
		for lgn in e['obj'].get_ligament_groups():
				var fg = e['obj'].get_ligaments( lgn )
				s = base + e['name'] + "/ligaments "
				ss =  s + "ligaments#" + lgn + "#shrink " + e['UID'] + " f" + shrink_range + " " + str(fg.get_shrink())
				send_methol( ss )
				ss =  s + "ligaments#" + lgn + "#stiffness " + e['UID'] + " f" + stiffness_range + " " + str(fg.get_stiffness())
				send_methol( ss )

func osc_menthol():
	var base = "assign FLUSH/"
	for e in ui.all_elements:
		osc_menthol_element(base, e)

func osc_menthol_set():
	if ui.selected != -1:
		var e = ui.all_elements[ ui.selected ]
		osc_menthol_element("set FLUSH/", e)

func osc_send_distances():
	
	if cam == null:
		print( "osc_send_distances NO CAM!!!" )
		return
	msg_address( "/distances" )
	var d = cam.global_transform.origin.length()
	msg_add_real( d )
	if len( previous_distances ) == 0:
		previous_distances.append( d )
		msg_add_real( 0 )
	else:
		msg_add_real( previous_distances[0] - d )
		previous_distances[0] = d
	var i = 1
	for e in cam_to_objects:
		if e == null:
			print( "osc_send_distances cam_to_objects NULL!!!" )
			continue
		d = (e.global_transform.origin - cam.global_transform.origin).length()
		msg_add_real(d  )
		if len( previous_distances ) <= i:
			previous_distances.append( d )
			msg_add_real( 0 )
		else:
			msg_add_real( previous_distances[i] - d )
			previous_distances[i] = d
		i += 1
	msg_send()

func _ready():
	if send_methol:
		ui = get_node("../ui")
	if send_distances:
		cam = get_node("../cam")
		cam_to_objects.append( get_node("../arm_ctlrs/base") )
		cam_to_objects.append( get_node("../arm_ctlrs/base/mid") )
		cam_to_objects.append( get_node("../arm_ctlrs/base/mid/tip") )

func _process(delta):
	
	if send_methol:
		if not methol_sent:
			if len( ui.all_elements ) > 0:
				osc_menthol()
				methol_sent = true
		elif Input.is_action_just_pressed("select_button"):
			osc_menthol_set()
		
	if send_gamepad and Input.get_connected_joypads().size() > 0:
		osc_gamepad()
	
	if send_distances:
		osc_send_distances()