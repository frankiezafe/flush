extends Node2D

export(float, 0, 100) var trans_speed = 3
export(float, 0, 100) var rot_speed = 15
export(float, 0, 100) var scale_speed = 5
export(float, 0, 100) var normal_speed = 5
export(float, 0, 100) var fov_speed = 10

export(String) var root_node = ""

var UID_PREFIX = 'device.'

var tmpl = null
var menu = null
var submenu = null
var marker = null
var marker_offsety = 7
var root = null
var cam_ctrl = null
var total_h = 0
var label_x = 50
var label_height = 25
var selected = 0
var group_selected = 0
var deadZone = 0.2
var normal_speed_curent = 0
var element_scale = 0
var all_elements = []
var dict_elements = {}

var transform_mode = "rotation" #[ rotation, translation ]

func scan_tree( element, skip = false ):
	
	var info = { 
		'name':element.name, 
		'obj':element, 
		'class':null, 
		'UID': UID_PREFIX + str(len(all_elements))
		}
	
	if element == self:
		return
	
	if not skip:
		if element.is_class( "Skin" ) and element.is_visible():
			info['class'] = "Skin"
		elif element.is_class( "Camera" ) and element.is_visible():
			info['class'] = "Camera"
		elif element.is_class( "Spatial" ) and element.is_visible():
			info['class'] = "Spatial"
		elif element.is_class( "Light" ) and element.is_visible():
			info['class'] = "Light"
	
	if info['class'] != null:
		all_elements.append( info )
		dict_elements[info['UID']] = info
	
	var childs = element.get_children()
	for c in childs:
		scan_tree( c )

func generate_menu():
	
	while menu.get_child_count() > 0:
		menu.remove_child( menu.get_child(0) )
	
	all_elements= []
	scan_tree( root, true )
	total_h = label_height * len( all_elements )
	var screen_size = get_viewport().size
	var lbl_y = screen_size.y * 0.5 - total_h * 0.5
	menu.position = Vector2( label_x, lbl_y )
	lbl_y = 0
	for i in all_elements:
		var label = tmpl.duplicate()
		label.text = i['name']
		label.visible = true
		label.rect_position = Vector2( 0, lbl_y )
		lbl_y += label_height
		menu.add_child( label )
	
	if selected >= len( all_elements ):
		selected = -1
	
	marker.position.y = menu.position.y + selected * label_height + marker_offsety

func generate_submenu():
	
	var e = all_elements[ selected ]
	
	if submenu != null:
		menu.remove_child( submenu )
	submenu = self.get_node( "submenu" ).duplicate()
	menu.add_child( submenu )
	submenu.visible = true
	
	var lbl_count = 0
	var lbl_y = 0
	for g in e['obj'].get_dot_groups():
		var label = tmpl.duplicate()
		label.text = g
		label.visible = true
		label.rect_position = Vector2( 0, lbl_y )
		lbl_y += label_height
		lbl_count += 1
		submenu.add_child( label )
	for g in e['obj'].get_fiber_groups():
		var label = tmpl.duplicate()
		label.text = g
		label.visible = true
		label.rect_position = Vector2( 0, lbl_y )
		lbl_y += label_height
		lbl_count += 1
		submenu.add_child( label )
	for g in e['obj'].get_ligament_groups():
		var label = tmpl.duplicate()
		label.text = g
		label.visible = true
		label.rect_position = Vector2( 0, lbl_y )
		lbl_y += label_height
		lbl_count += 1
		submenu.add_child( label )
	
	submenu.position.x = menu.get_child( selected ).get_end().x + label_x
	submenu.position.y = selected * label_height - int(lbl_count/2)*label_height

func _ready():
	
	root = get_node( root_node )
	if root == null:
		return
	
	menu = get_node( "menu" )
	marker = get_node( "marker" )
	tmpl = get_node( "tmpl" )
	cam_ctrl = root.get_node( "cam_ctrl" )
	
	generate_menu()
	
	menu.visible = false
	marker.visible = false

func analog_stabilisator( joyid, xaxis, yaxis ):
	var axis = Vector2(Input.get_joy_axis(joyid,xaxis),Input.get_joy_axis(joyid,yaxis))
	if abs( axis.x ) < deadZone:
		axis.x = 0
	elif axis.x < 0:
		axis.x = ( axis.x + deadZone ) / ( 1 - deadZone )
	elif axis.x > 0:
		axis.x = ( axis.x - deadZone ) / ( 1 - deadZone )
	if abs( axis.y ) < deadZone:
		axis.y = 0
	elif axis.y < 0:
		axis.y = ( axis.y + deadZone ) / ( 1 - deadZone )
	elif axis.y > 0:
		axis.y = ( axis.y - deadZone ) / ( 1 - deadZone )
	return axis

func _process(delta):
	
	if root == null:
		return
	
	var listening = true
	
	if Input.get_connected_joypads().size() > 0:
		if not menu.visible:
			if Input.is_action_just_pressed("gp_cross_up") or Input.is_action_just_pressed("gp_cross_down"):
				menu.visible = true
				marker.visible = true
			listening = false
		elif Input.is_action_just_pressed("gp_cross_right"):
			menu.visible = false
			marker.visible = false
			listening = false
	
	menu.position = Vector2( label_x, get_viewport().size.y * 0.5 - total_h * 0.5 )
	
	if listening:
		var smthchanged = false
		if Input.get_connected_joypads().size() > 0:
			if Input.is_action_just_pressed("gp_cross_up"):
				selected -= 1
				if selected < 0:
					selected = len( all_elements ) - 1
				smthchanged = true
			if Input.is_action_just_pressed("gp_cross_down"):
				selected += 1
				if selected >= len( all_elements ):
					selected = 0
				smthchanged = true
		if smthchanged:
			
			transform_mode = "rotation"
			marker.position.y = menu.position.y + selected * label_height + marker_offsety
			var e = all_elements[ selected ]
			if e['class'] == "Skin" or e['class'] == "Spatial": 
				element_scale = all_elements[ selected ]['obj'].scale.x
			else:
				element_scale = null
			
			if e['class'] == "Skin":
				generate_submenu()
			else:
				menu.remove_child( submenu )
				submenu = null
	
	if selected != -1:
		
		var e = all_elements[ selected ]
		
		# rotations
		var pos = analog_stabilisator( 0, JOY_ANALOG_LX, JOY_ANALOG_LY )
		if e['class'] == "Camera":
			e['obj'].fov += pos.y * fov_speed * delta
		else:
			if transform_mode == "rotation":
				e['obj'].rotation_degrees += Vector3( 0, pos.x, pos.y ) * rot_speed * delta
			elif transform_mode == "translation":
				e['obj'].translation += Vector3( pos.x, 0, pos.y ) * trans_speed * delta
		
		var l2 = Input.get_joy_axis(0,JOY_L2)
		var r2 = Input.get_joy_axis(0,JOY_R2)
		if e['class'] == "Camera":
			e['obj'].translation.y += pow(l2,2) * cam_ctrl.camspeed * delta - pow(r2,2) * cam_ctrl.camspeed * delta
		else:
			if transform_mode == "rotation":
				e['obj'].rotation_degrees.x += l2 * rot_speed * delta - r2 * rot_speed * delta
			elif transform_mode == "translation":
				e['obj'].translation.y += l2 * trans_speed * delta - r2 * trans_speed * delta
		
		# scale
		if element_scale != null:
			if Input.is_joy_button_pressed(0,JOY_L):
				element_scale += scale_speed * delta
			if Input.is_joy_button_pressed(0,JOY_R):
				element_scale -= scale_speed * delta
			if element_scale < 0.00001:
				element_scale = 0.00001
			e['obj'].scale = Vector3( element_scale,element_scale,element_scale )
		
		if e['class'] != "Skin":
			
			if Input.is_action_just_pressed("gp_X_button"):
				transform_mode = "rotation"
			if Input.is_action_just_pressed("gp_Y_button"):
				transform_mode = "translation"
		
		elif e['class'] == "Skin":
			if Input.is_action_just_pressed("gp_X_button"):
				e['obj'].display_ligaments = not e['obj'].display_ligaments
				generate_menu()
			if Input.is_action_just_pressed("gp_Y_button"):
				e['obj'].display_normals = not e['obj'].display_normals
				generate_menu()
			
			var decrease_nspeed = true
			if Input.is_joy_button_pressed(0,JOY_BUTTON_0): # Same as JOY_XBOX_A
				if normal_speed_curent < normal_speed:
					normal_speed_curent -= 1 * delta
					decrease_nspeed = false
				
			if Input.is_joy_button_pressed(0,JOY_BUTTON_1): # Same as JOY_XBOX_B
				if normal_speed_curent < normal_speed:
					normal_speed_curent += 1 * delta
					decrease_nspeed = false
			if decrease_nspeed:
				normal_speed_curent -= normal_speed_curent * 0.4 * delta
			e['obj'].normal_length += normal_speed_curent * delta
			if e['obj'].normal_length < 0:
				e['obj'].normal_length = 0