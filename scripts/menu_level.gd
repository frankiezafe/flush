extends Node2D

var label_tmpl = null
var highlight_tmpl = null
var marker = null

var elements = null
var prev_selected = null
var selected = null
var size = Vector2()
var labels = []

export(float) var offset_x = 25
export(float) var label_height = 25
export(float) var marker_offset_y = 7

func _ready():
	pass
	
func init( els, jump_to = 0 ):
	
	# clean up:
	label_tmpl = get_node( "label_tmpl" )
	highlight_tmpl = get_node( "highlight_tmpl" )
	marker = get_node( "marker" )
	
	label_tmpl.visible = false
	highlight_tmpl.visible = false
	
	while get_child_count() > 3:
		var cnum = get_child_count()
		for i in range(cnum):
			var c = get_child( i )
			if c == label_tmpl or c == marker or c == highlight_tmpl:
				continue
			remove_child( c )
			break
	
	elements = els
	
#	var info = { 
#		'name':element.name,
#		'lvl':0,
#		'parent': parent, 
#		'obj':element,
#		'class':null, 
#		'UID': 'device.' + str(UID),
#		'children': [],
#		'params' : { 'visible':true, 'rotation':null, 'translation':null, 'scale':null },
#		'last_param': null
#	}
	
	for i in range(len(elements)):
		var e = elements[i]
		var lbl = label_tmpl.duplicate()
		add_child( lbl )
		lbl.visible = true
		lbl.text = e['name']
		lbl.rect_position = Vector2( offset_x, label_height * i )
		if size.x < lbl.rect_size.x + offset_x:
			size.x = lbl.rect_size.x + offset_x
		labels.append( lbl )
	
	size.y = len(elements) * label_height
	
	marker.position = Vector2( 0, jump_to * label_height + marker_offset_y )
	marker.scale = Vector2( offset_x, 1 )
	
	selected = jump_to
	highlight()
	
	visible = true

func highlight():
	if prev_selected != null:
		labels[prev_selected].theme = label_tmpl.theme
		labels[prev_selected].set("custom_colors/font_color", label_tmpl.get("custom_colors/font_color") )
	if selected != null:
		labels[selected].theme = highlight_tmpl.theme
		labels[selected].set("custom_colors/font_color", highlight_tmpl.get("custom_colors/font_color") )
	prev_selected = selected

func get_size():
	return size

func get_element():
	return elements[selected]

func update_menu( delta, key ):
	
	if key == KEY_UP:
		selected -= 1
		if ( selected < 0 ):
			selected = len(elements) - 1
		highlight()
	elif key == KEY_DOWN:
		selected += 1
		if ( selected >= len(elements) ):
			selected = 0
		highlight()
	return elements[selected]
